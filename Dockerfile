FROM python:3.8-slim

ARG PACKAGE_VERSION
ARG GITLAB_TOKEN

COPY ./requirements.txt /requirements.txt
RUN pip install -r requirements.txt

CMD ["xrunner"]
